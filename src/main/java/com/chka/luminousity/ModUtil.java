package com.chka.luminousity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModUtil 
{
	private static org.apache.logging.log4j.Logger logger;
	
	public static Logger getLogger()
	{
		if(logger == null)
		{
			logger = LogManager.getFormatterLogger(Reference.MODID);
		}
		return logger;
	}
}

package com.chka.luminousity.init;

import com.chka.luminousity.ModUtil;
import com.chka.luminousity.common.items.ItemMortar;
import com.chka.luminousity.common.items.ItemReturnenJewel;
import com.chka.luminousity.common.items.scriptums.ceui.ItemCeuiScript;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModItems 
{
	public static Item returnenJewel, mortar, recordCalendula;
	public static Item ceuiScript;
	
	public static void init()
	{
		ceuiScript = new ItemCeuiScript("ceui_script", "ceui_script");
		returnenJewel = new ItemReturnenJewel("returnen_jewel", "returnen_jewel");
		mortar = new ItemMortar("mortar", "mortar");		
	}
	
	public static void register()
	{
		registerItem(returnenJewel);
		registerItem(ceuiScript);
		registerItem(mortar);
	}
	
	@SideOnly(Side.CLIENT)
	public static void registerRenders()
	{
		registerRender(returnenJewel);
		registerRender(ceuiScript);
		registerRender(mortar);
	}
	
	public static void registerItem(Item item)
	{
		GameRegistry.register(item);
		ModUtil.getLogger().info("Registered item "+ item.getRegistryName());	
	}
	
	@SideOnly(Side.CLIENT)
	public static void registerRender(Item item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
		ModUtil.getLogger().info("Register render for "+ item.getRegistryName());
	}
}

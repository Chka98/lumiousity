package com.chka.luminousity.init;

import com.chka.luminousity.ModUtil;
import com.chka.luminousity.common.blocks.BlockStarOre;
import com.chka.luminousity.common.blocks.BlockSunCrystal;
import com.chka.luminousity.common.tileentities.BlockRitualPlayer;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModBlocks 
{
	public static Block ritualPlayer, photonAssembler;
	public static Block silverleaf, peacebloom;
	public static Block starore, suncrystal;
	
	public static void init()
	{
		ritualPlayer = new BlockRitualPlayer("ritual_player", "ritual_player");
		//photonAssembler = new BlockPhotonAssembler("photon_assembler", "photon_assembler");
		// silverleaf = new BlockFlowerSilverleaf("silverleaf", "silverleaf");
		// peacebloom = new BlockFlowerPeacebloom("peacebloom", "peacebloom");
		starore = new BlockStarOre("star_ore", "star_ore");
		suncrystal = new BlockSunCrystal("sun_crystal", "sun_crystal");
	}
	
	public static void register()
	{
		registerBlock(ritualPlayer);
		//registerBlock(photonAssembler);
		// registerBlock(silverleaf);
		// registerBlock(peacebloom);
		registerBlock(starore);
		registerBlock(suncrystal);
	}
	
	@SideOnly(Side.CLIENT)
	public static void registerRenders()
	{
		registerRender(ritualPlayer);
		//registerRender(photonAssembler);
		// registerRender(silverleaf);
		// registerRender(peacebloom);
		registerRender(starore);
		registerRender(suncrystal);
	}

	public static void registerBlock(Block block)
	{
		GameRegistry.register(block);
		GameRegistry.register(new ItemBlock(block).setRegistryName(block.getRegistryName()));
		ModUtil.getLogger().info("Registered item "+ block.getRegistryName());
	}
	
	public static void registerBlock(Block block, ItemBlock itemBlock) 
	{
		GameRegistry.register(block);
		GameRegistry.register(itemBlock.setRegistryName(block.getRegistryName()));
		ModUtil.getLogger().info("Registered Block: " + block.getRegistryName());
	}
	
	@SideOnly(Side.CLIENT)
	public static void registerRender(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(block.getRegistryName(), "inventory"));
		ModUtil.getLogger().info("Register render for "+ item.getRegistryName());
	}
}

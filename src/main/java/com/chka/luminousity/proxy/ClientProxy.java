package com.chka.luminousity.proxy;

import com.chka.luminousity.init.ModBlocks;
import com.chka.luminousity.init.ModItems;

public class ClientProxy extends CommonProxy
{
	@Override
	public void registerRenders()
	{
		ModBlocks.registerRenders();
		ModItems.registerRenders();
	}
}

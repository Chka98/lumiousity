package com.chka.luminousity.common.blocks.flowers;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.block.BlockFlower;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockFlowerPeacebloom extends BlockFlower
{
	public BlockFlowerPeacebloom(String unlocalizedName, String registryName)
	{
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	
	@Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

	@Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
    
	@Override
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

	@Override
	public EnumFlowerColor getBlockType() 
	{
		// TODO Auto-generated method stub
		return EnumFlowerColor.RED;
	}
}

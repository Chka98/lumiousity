package com.chka.luminousity.common.blocks;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.ResourceLocation;

public class BlockStarOre extends Block
{
	public BlockStarOre(String unlocalizedName, String registryName)
	{
		super(Material.ROCK);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
}

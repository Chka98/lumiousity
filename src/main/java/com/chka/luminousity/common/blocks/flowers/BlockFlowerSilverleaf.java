package com.chka.luminousity.common.blocks.flowers;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.block.BlockFlower;
import net.minecraft.util.ResourceLocation;

public class BlockFlowerSilverleaf extends BlockFlower
{
	
	public BlockFlowerSilverleaf(String unlocalizedName, String registryName)
	{
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	
	@Override
	public EnumFlowerColor getBlockType() 
	{	
		return EnumFlowerColor.RED;
	}
}

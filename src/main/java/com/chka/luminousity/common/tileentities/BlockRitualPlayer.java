package com.chka.luminousity.common.tileentities;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.ResourceLocation;

public class BlockRitualPlayer extends Block
{
	public BlockRitualPlayer(String unlocalizedName, String registryName)
	{
		super(Material.WOOD);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	
	/*@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, new IProperty[]{ BlockRitualPlayer.HAS_RECORD });
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta) {
		return this.getDefaultState().withProperty(BlockRitualPlayer.HAS_RECORD, Boolean.valueOf(meta > 0));
	}
	
	@Override
	public int getMetaFromState(IBlockState state) {
		return (Boolean) state.getValue(BlockRitualPlayer.HAS_RECORD).booleanValue() ? 1 : 0;
	}*/
}

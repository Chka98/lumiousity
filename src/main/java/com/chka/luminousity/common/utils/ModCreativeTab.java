package com.chka.luminousity.common.utils;

import com.chka.luminousity.Reference;
import com.chka.luminousity.init.ModItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModCreativeTab 
{
	
	public static final CreativeTabs cTab = new CreativeTabs(CreativeTabs.getNextID(), Reference.MODID.toLowerCase())
	{
		@Override
		@SideOnly(Side.CLIENT)
        public ItemStack getTabIconItem()
		{
			return new ItemStack(ModItems.returnenJewel);
		}
	};
}
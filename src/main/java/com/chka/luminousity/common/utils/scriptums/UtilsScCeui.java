package com.chka.luminousity.common.utils.scriptums;

import java.util.Comparator;
import java.util.stream.Stream;

public enum UtilsScCeui 
{
	TEXT0(-1, "Unknown"),
	TEXT1(0, "I - Prologue:\nThis is a story from long, long ago.\nA tale from a far-off land.\nBeneath the brilliant sky me and you met by chance.\nWe where so happy together.\n�If we were to meet again just once more�\nIf I could hear your voice just once more�\nWhere am I to go now having lost you?�"),
	TEXT2(1, "II - The Cloud Goddess:\nVividly, languidly desiring light, Fate crumbled into a cruel sky.\nThe love that surrounded their relationship evoked jealousy in the Cloud Goddess and she couldn�t forgive them:\n�Say! Say! Love me!\nSay! Say! Look at me!\nSay! Say! Kneel before me!\nSay! Say! Above anyone else!\nSay! Say! I am the most beautiful in the world�!�\nIn madness, the goddess invaded his stolen body with her nail-marks.\nThe mirage of a tragedy.\nWithout changing, he continued to bring him, without knowing that his beloved had been stolen away�"),
	TEXT3(2, "III - The Witch of the Forest of Aphrodisiacs: \n�Welcome, I expected you would come.\nThis is the Aphrodisiac Forest.\nYou can forget everything here.\nNow, now, enjoy yourself!\nGood night, poor Sun God.\nGood night, until the Cloud Goddess returns.\nIn the depths of the gloomy forests, the Sun God awoke and a Wind nymph whispered to him:\n�It�s awful! The Goddess has gone to the boy!�"),
	TEXT4(3, "IV - The Cloud Goddess has�: \nThe thundering clouds, through an arrow of passion said �I am the muse. With this hand, I�ll destroy all of those things that are not in yours..! I cannot forgive you! I cannot forgive you because you didn�t choose me! My heart that keeps getting in the way and you, too� I�ll-,� Then, casting the poisoned arrow, it pierced through him, the boy�s beautiful eyes closed and he fell into sleep. Wordlessly as he was, the Sun God hadn�t told him about his love�"),
	TEXT5(4, "V - The Boy�s Memory: \nI recalled our first, dazzling meeting within me. Wherever you went, your freedom filled the sky with light. �What a radiant person� what a shining person�� How I wanted to know you better but now, that will never be able to happen. �Tomorrow, I want to talk more with that person� the boy yearned, spreading infinite wings."),
	TEXT6(5, "VI - The Time of His Demise: \n�Say! Say! Love me! Say! Say! Look at me! Say! Say! Kneel before me! Say! Say! I am the most beautiful in the world!� Running to them, the Sun God�s eyes widened, wishing that what he saw was a dream, the tragic image. Without opening his eyes, the boy gathered his cheeks in a final kiss. The Goddess vanished. The Sun God�s tears overflowed in him and he hardly cared, ah!"),
	TEXT7(6, "VII - The Sunlight of Restoration: \nLan, la, la, la, lan~ La, la, la, lan~ La, lan~ Before long, through a crack in the clouds- Lan, la, la, la, lan~ La, la, la, lan~ La, lan~ Yes, the light reached through �Smile� smile� just one more time look at me with that smile. We spent those sparse seasons together, floating. We didn�t meet each other to understand saddness. At least, bathed in the sunlight, smile. I will make a flower of you.�"),
	TEXT8(7, "VIII - Epilogue: \nThis is a story from long, long ago. A tale from a far-off land. �Calendula, For you, I- Calendula continue to shine. Calendula. Bloom to your very limits. Calendula. I- Calendula. I will become- Calendula. Become your sun. Calendula. Calendula. Calendula. Forever.");
	
	private int ID;
	private String text;
	
	public static final UtilsScCeui[] SCCEUI_LIST = Stream.of(values()).sorted(Comparator.comparing(UtilsScCeui::getID)).toArray(UtilsScCeui[]::new);
	
	private UtilsScCeui(int ID, String text) 
	{
		this.ID = ID;
		this.text = text;
	}
	
	public int getID() 
	{
		return this.ID;
	}
	
	public String getText() 
	{
		return this.text;
	}
	
	public static UtilsScCeui getTypeFromID(int id) 
	{	
		if(id < 0 || id >= SCCEUI_LIST.length)
		{
			id = 0;
		}
		
		return SCCEUI_LIST[id];
	}
	
	public static int getIndexFromID(int id) 
	{
		if(id < 0 || id >= SCCEUI_LIST.length)
		{
			id = 0;
		}
		
		return SCCEUI_LIST[id].getID();
	}
}

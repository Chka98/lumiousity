package com.chka.luminousity.common.items;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class ItemStarCrystal extends Item
{	
	public ItemStarCrystal(String unlocalizedName, String registryName)
	{
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) 
	{
		
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}

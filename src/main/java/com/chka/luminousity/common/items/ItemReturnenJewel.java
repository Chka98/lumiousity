package com.chka.luminousity.common.items;

import java.util.List;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class ItemReturnenJewel extends Item
{
	double x, y, z;
	boolean storedPosition;
	NBTTagCompound storedPositionTag;
	
	public ItemReturnenJewel(String unlocalizedName, String registryName)
	{
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	
	@Override
	public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) 
	{
		
	}
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) 
	{
		tooltip.add(TextFormatting.LIGHT_PURPLE + I18n.format("A teleporting stone!"));
		if(storedPosition)
		{
			tooltip.add(TextFormatting.GREEN + I18n.format("Destination: X : "+(int)x+" Y : "+(int)y+" Z : "+(int)z));
		}
		else
		{
			tooltip.add(TextFormatting.RED + I18n.format("No position set."));
		}
		super.addInformation(stack, playerIn, tooltip, advanced);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) 
	{
		if(worldIn.isRemote)
		{
			if(storedPosition)
			{
				playerIn.attemptTeleport(x, y, z);
				storedPosition = false;
				playerIn.sendMessage(new TextComponentString("Teleported to X :"+(int)x+" Y : "+(int)y+" Z : "+(int)z));
				
				if(!playerIn.isCreative())
				{
					return new ActionResult<ItemStack>(EnumActionResult.PASS, ItemStack.EMPTY);
				}
			}
			else
			{
				x = playerIn.posX;
				y = playerIn.posY;
				z = playerIn.posZ;
				storedPosition = true;
				playerIn.sendMessage(new TextComponentString("Postion set to X :"+(int)x+" Y : "+(int)y+" Z : "+(int)z));
			}
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}

package com.chka.luminousity.common.items;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.item.ItemRecord;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public class ItemRecordPandemonium extends ItemRecord
{
	public ItemRecordPandemonium(String unlocalizedName, SoundEvent sounds) 
	{
		super(unlocalizedName, sounds);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID.toLowerCase(), unlocalizedName));		
	}
}

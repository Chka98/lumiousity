package com.chka.luminousity.common.items.scriptums.ceui;

import java.util.List;
import java.util.Random;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.utils.ModCreativeTab;
import com.chka.luminousity.common.utils.scriptums.UtilsScCeui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

//Danke an Patrick~

public class ItemCeuiScript extends Item
{
	private Random random = new Random();
	
	public ItemCeuiScript(String unlocalizedName, String registryName)
	{
		this.setMaxStackSize(1);
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	@Override
	public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) 
	{
		getLoreIndex(stack);
		super.onCreated(stack, worldIn, playerIn);
	}
	
	private int getLoreIndex(ItemStack stack)
	{
		if(!stack.hasTagCompound())
			stack.setTagCompound(new NBTTagCompound());
		
		if(!stack.getTagCompound().hasKey("lore")) 
		{
			int chapter = this.random.nextInt((8 - 1) + 1) + 1;
			stack.getTagCompound().setInteger("lore", UtilsScCeui.getIndexFromID(chapter));
		}
		
		return stack.getTagCompound().getInteger("lore");
	}
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) 
	{
		tooltip.add(UtilsScCeui.getTypeFromID(getLoreIndex(stack)).getText());
	}
}
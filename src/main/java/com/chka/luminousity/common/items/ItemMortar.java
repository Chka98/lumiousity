package com.chka.luminousity.common.items;

import com.chka.luminousity.Reference;
import com.chka.luminousity.common.data.DataCalendulaMaterials;
import com.chka.luminousity.common.utils.ModCreativeTab;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class ItemMortar extends Item
{
	static int durability;
	
	public ItemMortar(String unlocalizedName, String registryName)
	{
		this.setUnlocalizedName(unlocalizedName);
		this.setCreativeTab(ModCreativeTab.cTab);
		this.setRegistryName(new ResourceLocation(Reference.MODID, registryName));
	}
	
	public static void decrementDurability(int x)
	{
		durability -= x;
	}
}

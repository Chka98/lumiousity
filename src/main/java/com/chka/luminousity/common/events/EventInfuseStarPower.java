package com.chka.luminousity.common.events;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class EventInfuseStarPower 
{
	public boolean infusementConditionalis(World worldIn, EntityPlayer playerIn)
	{
		if(!worldIn.isDaytime())
		{
			return true;
		}
		else
		{
			playerIn.sendMessage(new TextComponentString("The power of the stars declined you."));
		}
		return false;
	}
}

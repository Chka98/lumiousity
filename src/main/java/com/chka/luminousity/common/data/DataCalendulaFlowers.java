package com.chka.luminousity.common.data;

public enum DataCalendulaFlowers 
{
	//JUST THE WAU FLOWAS
	//Flower(skill, drop, rarity, biome, exp, mcexp);
	
	//VANILLA (Overworld)
	SILVERLEAF(1, 3, 1, "Plains", 5, 2),
	PEACEBLOOM(1, 3, 1, "Plains", 5, 2),
	/* NOT VANILLA */BLOODTHISTLE(1, 3, 1, "UNOBTAINABLE", 5, 2),
	EARTHROOT(15, 3, 2, "Mountain", 10, 8),
	MAGEROYAL(50, 3, 1, "Desert", 5, 2),
	SWIFTTHISTLE(0, 0, 0, "UNOBTAINABLE", 0, 0),
	BRIARTHORN(50, 3, 1, "Desert", 5, 2),
	BRUISEWEED(50, 3, 1, "Desert", 5, 2),
	STRANGLEKELP(85, 3, 1, "Ocean", 5, 2), //Murloc paper
	GRAVE_MOSS(95, 3, 1, "UNOBTAINABLE", 5, 2),
	WILD_STEELBLOOM(115, 3, 1, "Mountain", 5, 2),
	KINGSBLOOD(125, 3, 1, "Mountain", 5, 2),
	LIFEROOT(150, 3, 1, "Mountain", 5, 2),
	FADELEAF(150, 3, 1, "Plains", 5, 2),
	GOLDTHORN(150, 3, 1, "Plains", 5, 2),
	KHADGARS_WHISKER(180, 3, 1, "Plains", 5, 2),
	DRAGONS_TEETH(195, 3, 1, "Mountain", 5, 2),
	FIREBLOOM(205, 3, 1, "Desert", 5, 2),
	PURPLE_LOTUS(210, 3, 1, "Desert", 5, 2),
	WILDVINE(0, 0, 0, "UNOBTAINABLE", 5, 2),
	ARTHASTEARS(220, 3, 1, "I WILL NEVER GET THIS ACHIEVEMENT", 5, 2),
	SUNGRASS(230, 3, 1, "Desert", 5, 2),
	BLINDWEED(235, 3, 1, "Swamp", 5, 2),
	GHOST_MUSHROOM(245, 1, 2, "Swamp", 10, 8),
	GROOMSBLOOD(250, 3, 1, "Mountain", 5, 2),
	GOLDEN_SANSAM(260, 3, 1, "Mountain", 5, 2),
	DREAMFOIL(270, 3, 1, "Mountain", 5, 2),
	ICECAP(270, 3, 1, "Taiga", 5, 2),
	MOUNTAIN_SILVERSAGE(280, 3, 1, "Taiga", 5, 2),
	PLAGUEBLOOM(285, 3, 1, "Mushroom", 5, 2),
	BLACK_LOTUS(300, 1, 6, "Mountain", 30, 27),
	
	//BC
	FELWEED(300, 3, 1, "Hell", 5, 2),
	DREAMING_GLORY(315, 3, 1, "Hell", 5, 2),
	RAGVEIL(325, 3, 1, "Hell", 5, 2),
	TEROCORE(325, 3, 1, "Converted Hell", 5, 2),
	FLAME_CAP(335, 2, 5, "Hell", 5, 2),
	ANCIENT_LICHEN(340, 3, 1, "Hell", 5, 2),
	NETHERBLOOM(350, 3, 1, "Hell", 5, 2),
	NIGHTMARE_VINE(365, 3, 1, "Hell", 5, 2),
	MANA_THISTLE(375, 2, 5, "Converted Hell", 5, 2),
	FEL_LOTUS(0, 0, 0, "UNOBTAINABLE", 5, 2),
	
	//WotLK
	GOLD_CLOVER(350, 3, 1, "End", 5, 2),
	FIRE_LEAF(360, 3, 1, "Hell", 5, 2),
	TIGER_LILY(375, 3, 1, "End", 5, 2),
	VIOLET_ROSE(385, 3, 1, "End", 5, 2),
	FROZEN_HERB(400, 3, 1, "Snow", 5, 2),
	SERPENT_HERB(400, 3, 1, "Jungle", 5, 2),
	DEADTHORN(0, 0, 0, "UNOBTAINABLE", 0, 0),
	COLDBLOOM(425, 3, 1, "Snow", 5, 2),
	ICETHORN(435, 3, 1, "Snow", 5, 2),
	FROST_LOTUS(450, 2, 5, "Snow", 5, 2);
	
	private final int skill;   // required skill to pick this flower (may be deprecated)
	private final int drop; // amount of items that can be gained
	private final int rarity; // determines the chance to have additional items and the amout of the items from this type
	private final String biome; // the area it spawns in
	private final int exp; // experience for flower collection skill
	private final int mcexp; // minecraft experience
	
	DataCalendulaFlowers(int skill, int drop, int rarity, String biome, int exp, int mcexp) 
	{
        this.skill = skill;
        this.drop = drop;
        this.rarity = rarity;
        this.biome = biome;
        this.exp = exp;
        this.mcexp = mcexp;
    }
}

package com.chka.luminousity.common.magic;

public class MagicBase 
{
	private float damage;
	private int basecost;
	
	public MagicBase(float damage, int basecost)
	{
		this.damage = damage;
		this.basecost = basecost;
	}
}

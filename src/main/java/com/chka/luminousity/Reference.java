package com.chka.luminousity;

public class Reference 
{
    public static final String MODID = "luminousity";
    public static final String VERSION = "Behindi 1.0.1";
    public static final String NAME = "Luminousity";
    
    public static final String SERVER_PROXY_CLASS = "com.chka.luminousity.proxy.CommonProxy";
    public static final String CLIENT_PROXY_CLASS = "com.chka.luminousity.proxy.ClientProxy";
}

package com.chka.luminousity;

import com.chka.luminousity.common.world.WorldOreGeneration;
import com.chka.luminousity.init.ModBlocks;
import com.chka.luminousity.init.ModItems;
import com.chka.luminousity.proxy.CommonProxy;

import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MODID, version = Reference.VERSION, name = Reference.NAME)
public class Luminousity
{	
	
	//Happy New Year 2017~
	
	public static Item recordPandemonium, recordCalendula;
	public static Entity star_creeper;
	
	@Mod.Instance(Reference.MODID)
	public static Luminousity instance;
	
    @SidedProxy
    (serverSide = Reference.SERVER_PROXY_CLASS, clientSide = Reference.CLIENT_PROXY_CLASS)
    public static CommonProxy proxy;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	ModUtil.getLogger().info("Pre-Init started");
    	
    	//Sound
    	/*
    	 *  *LOCK*
    	ResourceLocation resource = new ResourceLocation(Reference.MODID.toLowerCase(), "calendula");
    	SoundEvent record_calendula = new SoundEvent(resource);
    	GameRegistry.register(record_calendula, resource);
    	
    	ResourceLocation resource2 = new ResourceLocation(Reference.MODID.toLowerCase(), "pandemonium");
    	SoundEvent record_pandemonium = new SoundEvent(resource2);
    	GameRegistry.register(record_pandemonium, resource2);
    	
    	//ItemsRecords 	
    	recordCalendula = new ItemRecordCalendula("record_calendula", record_calendula);
    	ModItems.registerItem(recordCalendula);
    	ModItems.registerRender(recordCalendula);
    	
    	recordPandemonium = new ItemRecordPandemonium("record_pandemonium", record_pandemonium);
    	ModItems.registerItem(recordPandemonium);
    	ModItems.registerRender(recordPandemonium);
    	*
    	*/
    	
    	//ItemsAuto
    	ModItems.init();
    	ModItems.register();
    	
    	//BlockAuto
    	ModBlocks.init();
    	ModBlocks.register();
    	
    	//Entities
    	
    	//WorldGen
    	
    	GameRegistry.registerWorldGenerator(new WorldOreGeneration(), 3);
    	
    	//Crafting
    	GameRegistry.addRecipe(new ItemStack(ModItems.ceuiScript),"   "," X ","   ",'X', new ItemStack(Items.APPLE));
    	GameRegistry.addRecipe(new ItemStack(ModItems.mortar),"X X"," X ","   ",'X', new ItemStack(Blocks.COBBLESTONE));
    	GameRegistry.addShapelessRecipe(new ItemStack(Items.DYE, 2, 1), new Object[] {new ItemStack(Blocks.RED_FLOWER), new ItemStack(ModItems.mortar)});
    	
    	proxy.registerRenders();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	ModUtil.getLogger().info("Init started");
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	ModUtil.getLogger().info("Post-Init started");
    }
}
